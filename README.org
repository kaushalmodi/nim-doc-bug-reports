1. Install ~nim~ using the latest devel as of today (2020-05-25)
2. ~cd prj_top~
3. ~nim doc --project top.nim~
4. Open ~htmldocs/top.html~ in a browser, and you will see this under Imports
   #+begin_example
   @@/@@/@@/@@/a/liba/liba, @@/@@/@@/@@/b1/b2/libb/libb, @@/libc/libc, libd/libd
   #+end_example
